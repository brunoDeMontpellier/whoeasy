use std::collections::HashMap;
use eframe::{egui, epi};
use crate::app::domain::whois_to_file;

#[path = "domain.rs"]
mod domain;

/// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[cfg_attr(feature = "persistence", derive(serde::Deserialize, serde::Serialize))]
#[cfg_attr(feature = "persistence", serde(default))] // if we add new fields, give them default values when deserializing old state
pub struct TemplateApp {
    extensions: HashMap<String, bool>,
    domains: Vec<String>,
    add_domain: String,
    brand: String,
}

impl Default for TemplateApp {
    fn default() -> Self {
        Self {
            extensions: HashMap::from([(String::from(".com"), true), (String::from(".fr"), true), (String::from(".org"), true), (String::from(".net"), false)]),
            brand: String::new(),
            domains: Vec::new(),
            add_domain: String::new(),
        }
    }
}

impl epi::App for TemplateApp {
    fn name(&self) -> &str {
        "whoeasy"
    }

    /// Called once before the first frame.
    fn setup(
        &mut self,
        _ctx: &egui::CtxRef,
        _frame: &epi::Frame,
        _storage: Option<&dyn epi::Storage>,
    ) {
        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        #[cfg(feature = "persistence")]
        if let Some(storage) = _storage {
            *self = epi::get_value(storage, epi::APP_KEY).unwrap_or_default()
        }
    }

    /// Called by the frame work to save state before shutdown.
    /// Note that you must enable the `persistence` feature for this to work.
    #[cfg(feature = "persistence")]
    fn save(&mut self, storage: &mut dyn epi::Storage) {
        epi::set_value(storage, epi::APP_KEY, self);
    }

    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::CtxRef, frame: &epi::Frame) {
        let Self { extensions, brand, domains, add_domain } = self;

        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            // The top panel is often a good place for a menu bar:
            egui::menu::bar(ui, |ui| {
                ui.menu_button("File", |ui| {
                    if ui.button("Quit").clicked() {
                        frame.quit();
                    }
                });
            });
            egui::global_dark_light_mode_switch(ui);
        });

        // Left panel where whe generate domain names.
        egui::SidePanel::left("side_panel").show(ctx, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui| {
                ui.heading("Noms de domaine");

                // Field to type the brand
                ui.label("Nom de la marque :");
                ui.text_edit_singleline(brand);

                if ui.button("Générer").clicked() {
                    domains.append(domain::generate_domain_name(&brand).as_mut());
                    brand.clear();
                }
                if ui.button("Vider").clicked() {
                    domains.clear();
                }

                for (i, domain) in domains.clone().iter().enumerate() {
                    if ui.button(domain).clicked() {
                        domains.remove(i);
                    }
                }
                ui.text_edit_singleline(add_domain);
                if ui.button("Ajouter ce nom de domaine").clicked() {
                    domains.push(add_domain.clone());
                    add_domain.clear();
                }

                if ui.button("Générer les whois").clicked() {
                    whois_to_file(&domains, &extensions);
                }
            });
        });

        // Central panel where we display the query's results.
        egui::CentralPanel::default().show(ctx, |ui| {
            egui::ScrollArea::both().show(ui, |_| {
                egui::SidePanel::left("Noms de domaine").show(ctx, |ui| {
                    ui.label("");
                    for domain in domains.clone() {
                        ui.label(domain);
                    }
                });
                for (ext, value) in extensions.clone() {
                    if value {
                        egui::SidePanel::left(ext.clone()).show(ctx, |ui| {
                            ui.label(&ext);
                            for _ in domains.clone() {
                                ui.label(value.to_string());
                            }
                        });
                    }
                }
            });
        });


        // Right panel where the extension selection happens.
        egui::SidePanel::right("Extensions").show(ctx, |ui| {
            egui::ScrollArea::vertical().show(ui, |ui|
                {
                    ui.heading("Extensions");
                    // Whe iterate through the extensions hashmap to display every option
                    for (ext, value) in extensions {
                        if ui.add(egui::SelectableLabel::new(*value, ext)).clicked() {
                            *value = !*value;
                        }
                    }
                });
        });
    }
}


