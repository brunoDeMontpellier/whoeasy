use std::collections::HashMap;
use whois_rust::{WhoIs, WhoIsLookupOptions};
use chrono::{Datelike, Timelike};
use debug_print::debug_eprintln;

pub fn generate_domain_name(brand: &str) -> Vec<String> {
    let words = Vec::from_iter(brand.split_whitespace());
    let mut domains = Vec::new();

    combine_words(&words, &mut domains, &String::new());


    domains
}

fn combine_words(words: &Vec<&str>, domains: &mut Vec<String>, domain: &String) {
    match words.len() {
        i if i == 1 => {
            domains.push(format!("{}{}", domain, words.first().unwrap()));
            if !domain.is_empty() {
                domains.push(format!("{}-{}", domain, words.first().unwrap()));
                domains.push(String::from(domain));
            }
        },

        i if i > 1 => {
            let word = words.first().cloned().unwrap();
            let words_without_first = words.as_slice()[1..i].to_vec();
            combine_words(&words_without_first, domains, &format!("{}{}", domain, word));
            if !domain.is_empty() {
                combine_words(&words_without_first, domains, &format!("{}-{}", domain, word));
            }
            combine_words(&words_without_first, domains, domain);
        },
        _ => (),
    };
}

pub fn whois_to_file (domains: &Vec<String>, extensions: &HashMap<String, bool>) {
    let whois = WhoIs::from_path("./json/servers.json").unwrap();

    let time = chrono::Local::now();
    let dir_name = format!("{}-{}-{}--{}h{}m{}s", time.year(), time.month(), time.day(), time.hour(), time.minute(), time.second());
    debug_eprintln!("dir_name : {}", dir_name);
    match std::fs::create_dir(&dir_name) {
        Ok(_) => println!("Folder created"),
        Err(e) => println!("Folder could not be created because : {}", e),
    };

    for domain in domains.clone() {
        for (ext, value) in extensions.clone() {
            if value {
                let full_domain = format!("{}{}", domain, ext);
                debug_eprintln!("{}", full_domain);
                let path = format!("./{}/{}", &dir_name, full_domain);
                debug_eprintln!("{}", path);
                let result = whois.lookup(WhoIsLookupOptions::from_string(full_domain).unwrap()).unwrap();
                match std::fs::write(path, result) {
                    Ok(_) => println!("File written"),
                    Err(e) => println!("Error is {}", e),
                };


            }
        }
    }
}
